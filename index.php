<?php
require("cnx.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ToDo List</title>
 <!----  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">--->
     <link rel="stylesheet" href="style.css">
  <!----  <style>
        .form-container {
            margin-top: 20px;
        }
        body {
    font-family: Arial, sans-serif;
    margin: 0;
    padding: 0;
    background-color: #f4f4f4;
}

    </style>   ---->
</head>
<body>
    <div class="container">
        <h1 id="todotitle" class="text-center">ToDo List</h1>
        <div class="form-container">
            <form action="index.php" method="post">
                <div class="form-group">
                    <label for="title">Task</label>
                    <input type="text" name="title" id="title" class="form-control" required>
                </div>
                <button id="add" type="submit" name="action" value="new" class="btn btn-primary" >Add</button>
            </form>
        </div>
        <ul class="list-group">
            <?php foreach ($taches as $tache): ?>
                <li class="list-group-item <?php echo $tache['done'] ? 'list-group-item-success' : 'list-group-item-warning'; ?>">
                    <?php echo htmlspecialchars($tache['title']); ?>
                    <form action="index.php" method="post" class="float-right">
                        <input type="hidden" name="id" value="<?php echo $tache['id']; ?>">
                        <button type="submit" name="action" value="toggle" class="btn btn-info">Done</button>
                        <button id="delete" type="submit" name="action" value="delete" class="btn btn-danger">delete</button>
                    </form>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</body>
</html>